#!/bin/env python3
from collections import namedtuple as nt
import math
from cmd import Cmd

class StackObject:
    valence = 0 # How many values dose this object need to return
    ocupation = [] # How many values is it supplied with
    init_vals = () # What values where passed to __init__
    sym = '&'

    def more(self):
        return len(self.ocupation) < self.valence

    def value(self):
        ocupied = len(self.ocupation)
        if ocupied == self.valence:
            return self.compute()
        else:
            return str(self)

    def new(self):
        return type(self)(*self.init_vals)

    def __call__(self,obj):
        if self.more():
            new = self.new()
            new.ocupation =  [*(self.ocupation), obj]
            return new
        raise Exception('Stack Object full up but more items where given')
 
    def __str__(self):
        return 'unnamed Stack Object'

    def __repr__(self):
        return str(self)

class Operation(StackObject):
    def __init__(self,sym,op):
        self.init_vals = (sym,op)
        self.sym = sym
        self.op = op
       
    def compute(self):
        ocupied_values = [item.value() for item in self.ocupation]
        return Number(self.op(*[float(value) for value in ocupied_values]))

    def __str__(self):
        l = ' '.join([self.sym,
                *[str(item) for item in self.ocupation],
                *['.' for _ in range(self.valence - len(self.ocupation))]])
        return f'({l})'

class Number(StackObject):
    def __init__(self,val):
        self.init_vals = (val)
        val = float(val)
        self.val = val
        self.sym = str(self)

    def compute(self):
        return self

    def __str__(self):
        n = round(self.val,5)
        if n.is_integer(): n = int(n)
        return str(n)

    def __float__(self):
        return self.val

    def new(self):
        return Number(self.val)

class Transform(Operation):
    valence = 1

class BiOp(Operation):
    valence = 2


consts = {
        'pi': Number(math.pi),
        'e' : Number(math.e),
        }

ops = { op.sym:op for op in [
    BiOp('+',lambda x,y:x+y),
    BiOp('-',lambda x,y:x-y),
    BiOp('*',lambda x,y:x*y),
    BiOp('/',lambda x,y:x/y),
    BiOp('**',lambda x,y:x**y),
    BiOp('log',lambda x,y: math.log(x,y)),
    Transform('exp',lambda x: math.e ** x),
    Transform('ln',math.log),
    Transform('sqrt',math.sqrt),
    Transform('sin',math.sin),
    Transform('cos',math.cos),
    Transform('tan',math.tan),
    Transform('asin',math.sin),
    Transform('acos',math.cos),
    Transform('atan',math.tan),
    ]}

############# CLI ##################

def apply(stack,obj):
    valence = obj.valence
    while len(stack) > 0 and obj.more():
        obj = obj(stack.pop())
    stack.append(obj)
    return stack

class Cli(Cmd):
    mkprompt = lambda s: s + ' > '
    prompt = mkprompt('')
    stack,future = [],[]

    def default(self,line):
        if ' ' in line:
            for word in line.split(' '):
                self.onecmd(word)
            return False

        if line in ops:
            obj = ops[line]
        elif line in consts:
            obj = consts[line]
        elif line == '<':
            if len(self.stack) < 2:
                return False
            a = self.stack.pop()
            b = self.stack.pop()
            if b.more():
                self.stack.append(a)
                obj = b
            else:
                self.stack.append(b)
                self.stack.append(a)
                return False
        elif line == '>':
            if len(self.stack) < 1:
                return False
            obj = self.stack.pop()
            if len(obj.ocupation) < 1:
                self.stack.append(obj)
                return False
            chi = obj.ocupation.pop()
            self.stack.append(obj)
            self.stack.append(chi)
            return False
        else:
            try:
                obj = Number(line)
            except:
                print('unknown symbol')
                return False

        self.stack = apply(self.stack,obj)

    def do_swap(self,line):
        if len(self.stack) >= 2:
            a = self.stack.pop()
            b = self.stack.pop()
            self.stack.append(a)
            self.stack.append(b)


    def do_rm(self,line):
        if len(self.stack) > 0:
            print('removed ',self.stack.pop())

    def do_un(self,line):
        if len(self.stack) > 0:
            obj = self.stack.pop()
            self.future.append(obj.new())
            print('undoing ',obj.sym)
            for chield in obj.ocupation:
                self.stack.append(chield)

    def do_re(self,line):
        if len(self.future) > 0: 
            obj = self.future.pop()
            self.stack = apply(self.stack,obj)
            
    def do_ls(self,line):
        self.do_listStack(line)


    def postcmd(self,stop,line):
        for obj in self.stack:
            print(obj.value(),end=' ')
        print()
        if stop:
            return True

    def emptyline(self):
        pass

    def do_listStack(self,line):
        for obj in self.stack:
            print(obj)

    def do_listFuture(self,line):
        for obj in self.future:
            print(obj)

    def do_listConstants(self,line):
        for key in consts:
            print(key,consts[key])

    def do_EOF(self,line):
        return True


if __name__ == '__main__':
    cli = Cli()
    cli.cmdloop()
